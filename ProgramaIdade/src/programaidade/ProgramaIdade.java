/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package programaidade;

import java.util.Scanner;

/**
 *
 * @author nossa
 */
public class ProgramaIdade {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Scanner tec = new Scanner(System.in);
        System.out.print("Digite o ano de nascimento ");
        int nasci = tec.nextInt();
        int age = 2023 - nasci;
        System.out.println("Sua idade é " + age);
        if (age >= 18){
            System.out.println("Maior de Idade");
        } else {
            System.out.println("Menor de idade");
        }
    }
    
}
